﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Player.Api.Dto
{
    public class TrackWithLinks
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Duration { get; set; }

        public byte NumberInRelease { get; set; }

        public string Genre { get; set; }

        public string Artist { get; set; }

        public string Release { get; set; }
    }
}
