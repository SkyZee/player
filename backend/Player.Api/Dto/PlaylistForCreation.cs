﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Player.Api.Dto
{
    public class PlaylistForCreation
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int[] Tracks { get; set; }
    }
}
