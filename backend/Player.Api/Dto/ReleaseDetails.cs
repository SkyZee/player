﻿using System;
using System.Collections.Generic;

namespace Player.Api.Dto
{
    public class ReleaseDetails
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime ReleaseDate { get; set; }

        public int LabelId { get; set; }

        public string LabelName { get; set; }

        public string CoverImgSrc { get; set; }

        public ICollection<TrackForRelease> Tracks { get; set; }
    }
}
