﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Player.Api.Dto
{
    public class TrackForRelease
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ArtistId { get; set; }

        public string Artist { get; set; }

        public int LabelId { get; set; }

        public string Label { get; set; }

        public decimal Duration { get; set; }

        public int ReleaseId { get; set; }

        public byte NumberInRelease { get; set; }

        public int GenreId { get; set; }

        public string Genre { get; set; }    
        
        public string Src { get; set; }

        public string AlbumArtSrc { get; set; }
    }
}
