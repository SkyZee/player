﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Player.Api.Dto
{
    public class ArtistDetails
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<TrackForRelease> Tracks { get; set; }
    }
}
