﻿using System.Collections.Generic;

namespace Player.Api.Dto
{
    public class HomeModel
    {
        public List<PlaylistForList> Playlists { get; set; }
    }
}
