﻿using System.Collections.Generic;

namespace Player.Api.Dto
{
    public class GenreDetails
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<TrackForRelease> Tracks { get; set; }
    }
}
