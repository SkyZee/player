﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Player.Api.Dto
{
    public class ArtistForCreation
    {
        public string Name { get; set; }
    }
}
