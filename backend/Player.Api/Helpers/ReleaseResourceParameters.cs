﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Player.Api.Helpers
{
    public class ReleaseResourceParameters
    {
        //private IConfiguration config;
        private int maxPageSize = 20;

        //public ReleaseResourceParameters(IConfiguration configuration)
        //{
        //    config = configuration;
        //    maxPageSize = int.Parse(config["Releases:MaxPageSize"]);
        //}        

        public int PageNumber { get; set; } = 1;

        private int _pageSize = 10;        

        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }
    }
}

        