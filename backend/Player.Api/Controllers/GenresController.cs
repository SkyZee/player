﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Player.Api.Db;
using Player.Api.Dto;
using Player.Api.Repositories;
using Player.Domain;

namespace Player.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Genres")]
    public class GenresController : Controller
    {
        private GenericRepository<Genre> genreRepository;
        private PlayerDbContext ctx;
        private IMapper mapper;

        public GenresController(GenericRepository<Genre> repository, PlayerDbContext context, IMapper mapper)
        {
            genreRepository = repository;
            ctx = context;
            this.mapper = mapper;
        }

        //[HttpGet("genres")]
        public IActionResult GetGenres()
        {
            var genres = genreRepository.All();

            return Ok(genres);
        }

        [HttpGet("{genreId}")]
        public IActionResult GetGenre(int genreId)
        {
            //var genre = ctx.Genres.Include(g => g.Tracks).AsNoTracking().FirstOrDefault(g => g.Id == genreId).Include(;
            var genre = ctx.Genres.Include(g => g.Tracks)
                .ThenInclude(t => t.Artist)
                .Include(g => g.Tracks)
                .ThenInclude(t => t.Release)
                .FirstOrDefault(g => g.Id == genreId);

            var genreDetails = mapper.Map<GenreDetails>(genre);
            return Ok(genreDetails);
        }
    }
}