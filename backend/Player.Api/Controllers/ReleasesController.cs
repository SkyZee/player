﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Player.Api.Db;
using Player.Api.Dto;
using Player.Api.Helpers;
using Player.Api.Repositories;
using Player.Domain;

namespace Player.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Releases")]
    public class ReleasesController : Controller
    {
        private GenericRepository<Release> releaseRepository;
        private PlayerDbContext ctx;
        private IMapper mapper;

        const int maxReleasePageSize = 50; // TODO: move to settings

        public ReleasesController(GenericRepository<Release> releaseRepository, PlayerDbContext context, IMapper mapper)
        {
            this.releaseRepository = releaseRepository;
            ctx = context;
            this.mapper = mapper;
        }

        public async Task<IActionResult> GetReleases(ReleaseResourceParameters releaseResourceParameters)
        {
            var releases = await ctx.Releases
                .Include(r => r.Label)
                .Include(r => r.Tracks).ThenInclude(tr => tr.Genre)
                .Include(r => r.Tracks).ThenInclude(t => t.Artist)
                .Skip(releaseResourceParameters.PageSize * (releaseResourceParameters.PageNumber - 1))
                .Take(releaseResourceParameters.PageSize)
                .ToListAsync();
            return Ok(mapper.Map<List<ReleaseDetails>>(releases));
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetRelease(int id)
        {
            var release = await ctx.Releases
                .Include(r => r.Label)
                .Include(r => r.Tracks).ThenInclude(t => t.Genre)
                .Include(r => r.Tracks).ThenInclude(t => t.Artist)
                .FirstOrDefaultAsync(r => r.Id == id);

            if (release == null)
            {
                return NotFound();
            }
            var releaseDetails = mapper.Map<ReleaseDetails>(release);
            return Ok(releaseDetails);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteRelease(int id)
        {
            var release = releaseRepository.FindByKey(id);
            if (release == null)
            {
                return NotFound();
            }

            try
            {
                releaseRepository.Delete(id);
            }
            catch (Exception e)
            {
                throw new Exception($"Deleting release {id} failed");
            }

            return NoContent();            
        }

    }
}