﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Player.Api.Db;
using Player.Api.Dto;
using Player.Api.Repositories;
using Player.Domain;

namespace Player.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Tracks")]
    public class TracksController : Controller
    {
        private GenericRepository<Track> trackRepository;
        private IMapper mapper;

        public TracksController(GenericRepository<Track> repo, IMapper mapper)
        {
            trackRepository = repo;
            this.mapper = mapper;
        }

        public IActionResult GetTracks()
        {
            var tracks = trackRepository.All();
            return Ok(tracks);
        }


        [HttpGet("{id}")]
        public IActionResult GetTrack(int id)
        {
            var track = trackRepository.FindByKey(id);
            return Ok(mapper.Map<TrackForRelease>(track));
        }
    }
}