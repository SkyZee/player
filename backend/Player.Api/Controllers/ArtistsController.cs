﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Player.Api.Db;
using Player.Api.Dto;
using Player.Api.Repositories;
using Player.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Player.Api.Controllers
{

    [Produces("application/json")]
    [Route("api/Artists")]
    [ApiController]
    public class ArtistsController : ControllerBase
    {
        private GenericRepository<Artist> artistRepository;
        private PlayerDbContext ctx;
        private IMapper mapper;

        public ArtistsController(GenericRepository<Artist> repository, PlayerDbContext context, IMapper mapper)
        {
            artistRepository = repository;
            ctx = context;
            this.mapper = mapper;
        }

        public async Task<IActionResult> GetArtists()
        {
            var artists = await ctx.Artists
                .Include(a => a.Tracks).ThenInclude(t => t.Release)
                .Include(a => a.Tracks).ThenInclude(t => t.Artist).ToListAsync();
            var result = mapper.Map<List<ArtistDetails>>(artists);
            return Ok(result);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetArtist(int id)
        {
            var artist = await ctx.Artists
                .Include(a => a.Tracks).ThenInclude(t => t.Release)
                .Include(a => a.Tracks).ThenInclude(t => t.Artist)
                .FirstOrDefaultAsync(a => a.Id == id);
            if (artist == null)
            {
                return NotFound();
            }

            return Ok(mapper.Map<ArtistDetails>(artist));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteArtist(int id)
        {
            var artist = artistRepository.FindByKey(id);

            if (artist == null)
            {
                return NotFound();
            }

            try
            {
                artistRepository.Delete(id);
            }
            catch 
            {
                throw new Exception($"Deleting artist {id} failed");
            }
            return NoContent();
        }
    }
}