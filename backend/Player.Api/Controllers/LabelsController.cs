﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Player.Api.Db;
using Player.Api.Repositories;
using Player.Domain;

namespace Player.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Labels")]
    public class LabelsController : Controller
    {
        private GenericRepository<Label> labelRepository;
        private PlayerDbContext ctx;

        public LabelsController(GenericRepository<Label> labelRepository, PlayerDbContext context)
        {
            this.labelRepository = labelRepository;
            this.ctx = context;
        }

        public IActionResult GetLabels()
        {
            return Ok(labelRepository.All());
        }

        [HttpGet("{id}")]
        public IActionResult GetLabel(int id)
        {
            var label = ctx.Labels.Include(l => l.Releases).FirstOrDefault(l => l.Id == id);

            if (label == null)
            {
                return NotFound();
            }

            return Ok(label);
        }


        [HttpPost]
        public IActionResult CreateLabel([FromBody] Label label)
        {
            labelRepository.Insert(label);
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteLabel(int id)
        {
            var label = labelRepository.FindByKeyAsync( id);
            if (label == null)
            {
                return NotFound();
            }

            labelRepository.Delete(id);
            return NoContent();
        }
    }
}