﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Player.Api.Db;
using Player.Api.Dto;
using Player.Domain;

namespace Player.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Playlists")]
    public class PlaylistsController : Controller
    {
        private PlayerDbContext ctx;
        private IMapper mapper;

        public PlaylistsController(IMapper mapper, PlayerDbContext ctx)
        {
            this.mapper = mapper;
            this.ctx = ctx;
        }

        public async Task<IActionResult> GetPlaylists()
        {
            var playlists = await ctx.Playlists
                .Include(p => p.PlaylistTracks).ThenInclude(pt => pt.Track)
                .ThenInclude(t => t.Artist)
                .Include(p => p.PlaylistTracks).ThenInclude(pt => pt.Track)
                .ToListAsync();
            var playlistsDto = mapper.Map<List<PlaylistForList>>(playlists);
            return Ok(playlistsDto);
        }

        [HttpPost]
        public IActionResult CreatePlaylist([FromBody] PlaylistForCreation p)
        {
            if (p == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var playlist = mapper.Map<Playlist>(p);
            ctx.Playlists.Add(playlist);
            if (p.Tracks != null)
            {
                var order = 1;
                foreach (var tid in p.Tracks)
                {
                    ctx.PlaylistTrack.Add(new PlaylistTrack { TrackId = tid, Playlist = playlist, Order = order });
                    order++;
                }
            }
            ctx.SaveChanges();
            return Ok(playlist);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeletePlaylist(int? id)
        {
            if (id == null)
            {
                return NotFound("Not a valid playlist ID");
            }
            var playlist = ctx.Playlists.SingleOrDefault(p => p.Id == id);
            if (playlist == null)
            {
                return NotFound();
            }
            ctx.Playlists.Remove(playlist);
            ctx.SaveChanges();
            return Ok();
        }
    }
}