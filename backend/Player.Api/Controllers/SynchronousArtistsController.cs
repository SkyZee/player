﻿using Microsoft.AspNetCore.Mvc;
using Player.Api.Repositories;
using Player.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Player.Api.Controllers
{
    [Route("api/synchronousartists")]
    [ApiController]
    public class SynchronousArtistsController : ControllerBase
    {
        private GenericRepository<Artist> artistsRepository;

        public SynchronousArtistsController(GenericRepository<Artist> artistsRepository)
        {
            this.artistsRepository = artistsRepository;
        }

        public IActionResult GetArtists()
        {
            var artists = artistsRepository.All();
            return Ok(artists);
        }
    }
}
