﻿using Microsoft.EntityFrameworkCore;
using Player.Api.Db;
using Player.Api.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Player.Api.Repositories
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal PlayerDbContext ctx;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(PlayerDbContext context)
        {
            ctx = context;
            dbSet = context.Set<TEntity>();
        }

        public IEnumerable<TEntity> All()
        {
            ctx.Database.ExecuteSqlCommand("WAITFOR DELAY '00:00:02';");
            return dbSet.AsNoTracking().ToList();
        }

        public async Task<IEnumerable<TEntity>> AllAsync()
        {
            ctx.Database.ExecuteSqlCommand("WAITFOR DELAY '00:00:02';");
            return await dbSet.AsNoTracking().ToListAsync();
        }

        public IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            IEnumerable<TEntity> results = dbSet.AsNoTracking()
                .Where(predicate).ToList();
            return results;
        }

        public async Task<TEntity> FindByKeyAsync(int id)
        {
            Expression<Func<TEntity, bool>> lambda = Utilities.BuildLambdaForFindByKey<TEntity>(id);
            return await dbSet.AsNoTracking().SingleOrDefaultAsync(lambda);
        }

        public TEntity FindByKey(int id)
        {
            Expression<Func<TEntity, bool>> lambda = Utilities.BuildLambdaForFindByKey<TEntity>(id);
            return dbSet.AsNoTracking().SingleOrDefault(lambda);
        }

        public void Insert(TEntity entity)
        {
            dbSet.Add(entity);
            ctx.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            dbSet.Attach(entity);
            ctx.Entry(entity).State = EntityState.Modified;
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = FindByKey(id);
            dbSet.Remove(entity);
            ctx.SaveChanges();
        }


        // TODO
        public PagedList<TEntity> GetPagedList()
        {
            var pageNumber = 10;
            var pageSize = 20;
            var collectionBeforePaging = dbSet.AsQueryable();
            return PagedList<TEntity>.Create(collectionBeforePaging, pageNumber, pageSize);
        }

    }
}
