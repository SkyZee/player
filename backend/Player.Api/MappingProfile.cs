﻿using AutoMapper;
using Player.Api.Dto;
using Player.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Player.Api
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Playlist, PlaylistForCreation>();
            CreateMap<PlaylistForCreation, Playlist>();
            // CreateMap<Playlist, PlaylistForList>();

            CreateMap<PlaylistTrack, TrackForRelease>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Track.Name))
                .ForMember(dest => dest.Src, opt => opt.MapFrom(src => src.Track.Url))
                .ForMember(dest => dest.ReleaseId, opt => opt.MapFrom(src => src.Track.Release.Id))
                .ForMember(dest => dest.Artist, opt => opt.MapFrom(src => src.Track.Artist.Name));

            CreateMap<Playlist, PlaylistForList>().ForMember(dest => dest.Tracks, opt => opt.MapFrom(src => src.PlaylistTracks));

            CreateMap<Track, TrackForRelease>()
                .ForMember(dest => dest.Artist, opt => opt.MapFrom(src => src.Artist.Name))
                .ForMember(dest => dest.Genre, opt => opt.MapFrom(src => src.Genre.Name))
                .ForMember(dest => dest.Src, opt => opt.MapFrom(src => src.Url))
                .ForMember(dest => dest.AlbumArtSrc, opt => opt.MapFrom(src => src.Release.CoverImgSrc));

            CreateMap<Genre, GenreDetails>();

            CreateMap<Release, ReleaseDetails>();
            
        }
    }
}
