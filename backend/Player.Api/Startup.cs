﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Player.Api.Db;
using Player.Api.Repositories;
using Player.Domain;

namespace Player.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //TODO: services.addjson
            var connStr = Configuration["Data:DefaultConnection:PlayerSqlDbConnectionString"];

            services.AddDbContext<PlayerDbContext>(c => c.UseSqlServer(connStr))
                .AddIdentity<PlayerUser, IdentityRole>()
                .AddEntityFrameworkStores<PlayerDbContext>();

            services.AddScoped<GenericRepository<Track>>();
            services.AddScoped<GenericRepository<Artist>>();
            services.AddScoped<GenericRepository<Genre>>();
            services.AddScoped<GenericRepository<Label>>();
            services.AddScoped<GenericRepository<Playlist>>();
            services.AddScoped<GenericRepository<Release>>();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    cfg => cfg.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
            
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            services.AddAutoMapper();
            services.AddMvc().AddJsonOptions(o => o.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");

            app.UseMvc();
            app.UseDefaultFiles();
            app.UseStaticFiles();            
        }
    }
}
