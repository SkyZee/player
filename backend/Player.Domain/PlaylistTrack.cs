﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Player.Domain
{
    public class PlaylistTrack
    {
        [Key]
        public int Id { get; set; }

        public int PlaylistId { get; set; }

        public virtual Playlist Playlist { get; set; }

        public int TrackId { get; set; }

        public virtual Track Track { get; set; }

        public int Order { get; set; }
    }
}
