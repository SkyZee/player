﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Player.Domain
{
    public class Release
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime ReleaseDate { get; set; }

        public Label Label { get; set; }

        public virtual ICollection<Track> Tracks { get; set; }

        public string CoverImgSrc { get; set; }
    }
}
