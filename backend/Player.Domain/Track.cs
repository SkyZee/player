﻿using System;
using System.Collections.Generic;

namespace Player.Domain
{
    public class Track
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Duration { get; set; }

        public byte NumberInRelease { get; set; }

        public virtual ICollection<PlaylistTrack> PlaylistTracks { get; set; }

        public virtual Genre Genre { get; set; }

        public virtual Artist Artist { get; set; }

        public virtual Release Release { get; set; }

        public string Url { get; set; }
    }
}
