﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Player.Domain
{
    public class Playlist
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<PlaylistTrack> PlaylistTracks { get; set; }

    }
}
