﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Player.Domain
{
    public class Label
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Release> Releases { get; set; }
    }
}
