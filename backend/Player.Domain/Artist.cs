﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Player.Domain
{
    public class Artist
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Track> Tracks { get; set; }
    }
}
