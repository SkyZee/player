import { Component, OnInit } from '@angular/core';
import { Howl } from 'howler';
import { ITrack } from '../music-library/shared/track.model';
import { Store, select } from '@ngrx/store';
import { State } from '../state/app.state';
import * as fromPlayer from '../music-library/state/music-library.reducer';
import * as playerActions from '../music-library/state/music.library.actions';

import { MatSliderChange } from '@angular/material';

@Component({
  selector: 'player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  Direction = Direction; // just for using enum Direction in a template
  sound: Howl;
  queue: ITrack[];
  currentTrack: ITrack;
  isTrackUploaded: boolean = false;

  duration: string;
  timer: string;
  isMuted: boolean = false;
  isPlaying: boolean = false;

  progress: number;
  bufferValue: number = 100;

  constructor(private store: Store<State>) { }

  ngOnInit() {
    // this.store.pipe(select('library')).subscribe(
    //   lib => {
    //     this.currentTrack = lib.currentTrack;
    //     if (lib.currentTrack) {
    //       this.playNew(this.currentTrack.src);
    //     }
    //   }
    // );
  }

  playNew(path: string) {
    if (this.sound) {
      this.sound.unload();
    }
    this.sound = new Howl({
      src: [path],
      // src: ['https://fex.net/load/864965720765/958652025'],
      html5: true,
      onplay: () => {
        this.duration = this.formatTime(this.sound.duration());
        requestAnimationFrame(() => this.step());
      },
      onend: () => this.onEnd()
    });
    this.sound.play();
    this.isPlaying = true;
  }

  onEnd() {
    this.store.dispatch(new playerActions.PlayNextTrack());
  }

  onPlayProgress() {

  }

  step(): any {
    let seek = +this.sound.seek();
    this.progress = seek / this.sound.duration() * 100 || 0;
    this.timer = this.formatTime(Math.round(seek));
    if (this.sound.playing()) {
      requestAnimationFrame(() => this.step());
    }
  }

  skip(direction: Direction) {
    if (direction == Direction.Next) {
      this.store.dispatch(new playerActions.PlayNextTrack())
    } else if (direction == Direction.Prev) {
      this.store.dispatch(new playerActions.PlayPrevTrack())
    }
  }

  togglePlay() {
    if (this.isPlaying) {
      this.isPlaying = false;
      if (this.sound) {
        this.sound.pause();
      }
    } else {
      this.isPlaying = true;
      if (this.sound) {
        this.sound.play()
      }
    }
  }

  toggleMute() {
    if (this.isMuted) {
      this.isMuted = false;
      if (this.sound) {
        this.sound.mute(false);
      }
    } else {
      this.isMuted = true;
      if (this.sound) {
        this.sound.mute(true);
      }
    }
  }

  seek() {

  }

  onSliderChange(e: MatSliderChange) {
    console.log(e.value);
    this.progress = e.value;
    this.sound.seek(this.sound.duration() / 100 * e.value);
  }

  onInputChange(e: MatSliderChange) {
    console.log(e.value);
  }

  formatTime(secs: number): string {
    secs = Math.round(secs);
    var minutes = Math.floor(secs / 60) || 0;
    var seconds = (secs - minutes * 60) || 0;

    return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
  }
}

enum Direction {
  Prev,
  Next
}
