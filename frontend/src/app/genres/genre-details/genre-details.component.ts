import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Genre } from '../genre';
import { GenreService } from '../genre.service';
import { ITrack } from 'src/app/music-library/shared/track.model';
import * as playerActions from '../../music-library/state/music.library.actions';
import { Store } from '@ngrx/store';
import { State } from 'src/app/state/app.state';

@Component({
  selector: 'app-genre-details',
  templateUrl: './genre-details.component.html',
  styleUrls: ['./genre-details.component.css']
})
export class GenreDetailsComponent implements OnInit {

  genre: Genre;
  sub: Subscription;

  constructor(private genreService : GenreService, private route: ActivatedRoute, private store: Store<State>) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(
      params => {
        let id = +params['id'];
        this.getGenre(id);
      }
    )
  }

  getGenre(id: number) {
    this.genreService.getGenre(id).subscribe(
      genre => this.genre = genre);    
  }

  addTrackToQueue(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueue([track]));
  }
  
  addAndPlayTrack(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueueAndStartPlaying([track]));
  }

}
