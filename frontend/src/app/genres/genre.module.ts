import { NgModule } from "@angular/core";
import { GenreListComponent } from "./genre-list/genre-list.component";
import { GenreDetailsComponent } from "./genre-details/genre-details.component";
import { GenreService } from "./genre.service";
import { SharedModule } from "../shared/shared.module";
import { RouterModule } from "@angular/router";
import { MaterialModule } from "../material.module";

@NgModule({
    imports: [
        SharedModule,
        RouterModule,
        MaterialModule
    ],
    declarations: [
        GenreListComponent,
        GenreDetailsComponent
    ],
    providers: [
        GenreService
    ]
})
export class GenreModule { }
