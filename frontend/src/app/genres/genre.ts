import { ITrack } from "../music-library/shared/track.model";

export interface Genre {
    id: number;
    name: string;
    tracks: ITrack[];
}