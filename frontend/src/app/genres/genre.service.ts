import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { Observable, of, throwError } from "rxjs";
import { catchError, tap } from 'rxjs/operators';
import { Genre } from "./genre";
import { map } from 'rxjs/operators';
import { environment } from "src/environments/environment";

@Injectable()
export class GenreService {
    private baseUrl = environment.endpointBase + 'genres';
    genres: Genre[];

    constructor(private http: HttpClient) { }

    getGenres(): Observable<Genre[]> {
        if (this.genres) {
            return of(this.genres);
        }
        return this.http.get<Genre[]>(this.baseUrl)
            .pipe(
                tap(data => this.genres = data)
            );
    }

    getGenre(id: number): Observable<Genre> {
        return this.http.get<Genre>(this.baseUrl+"/"+id).pipe(tap(m => m));
    }

    private handleError(err) {

        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
          errorMessage = `An error occurred: ${err.error.message}`;
        } else {
          errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
        }
        console.error(err);
        return throwError(errorMessage);
      }
}