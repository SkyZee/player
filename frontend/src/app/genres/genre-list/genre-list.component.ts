import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromLibrary from "../../music-library/state/music-library.reducer";
import * as playerActions from '../../music-library/state/music.library.actions';
import { takeWhile } from 'rxjs/operators';
import { Genre } from '../genre';
import { GenreService } from '../genre.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styleUrls: ['./genre-list.component.css']
})
export class GenreListComponent implements OnInit {
  
  genres$: Observable<Genre[]>;

  constructor(private genreService: GenreService) { }

  ngOnInit() {
    this.genres$ = this.genreService.getGenres();
  }
}
