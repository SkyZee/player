import { Injectable } from "@angular/core";
import { GenreService } from "./genre.service";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Observable } from "rxjs";
import { Action } from "@ngrx/store";
import * as libraryActions from "../music-library/state/music.library.actions";
import { mergeMap, map } from "rxjs/operators";
import { Genre } from "./genre";

@Injectable()
export class GenreEffects {

    constructor(private actions$: Actions, private genreService: GenreService) {}
    
    @Effect()
    loadGenres$: Observable<Action> = this.actions$.pipe(
        ofType(libraryActions.PlayerActionTypes.LoadGenres),
        mergeMap(action => this.genreService.getGenres()
            .pipe(
                map((genres: Genre[]) => (new libraryActions.LoadGenresSuccess(genres)))
            )
        )
    )
}