import { Component, OnInit, ViewChild } from '@angular/core';
import { Release } from '../Release';
import { ActivatedRoute } from '@angular/router';
import { MatSort,
  MatTableDataSource,
  // MatSnackBar
} from '@angular/material';
import { ITrack } from 'src/app/music-library/shared/track.model';
import { FooterComponent } from 'src/app/footer/footer.component';
import * as playerActions from '../../music-library/state/music.library.actions';
import { State } from '../../state/app.state';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-release-details',
  templateUrl: './release-details.component.html',
  styleUrls: ['./release-details.component.css']
})
export class ReleaseDetailsComponent implements OnInit {

  release: Release;
  displayedColumns: string[];
  dataSource: MatTableDataSource<ITrack>;

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    // public snackBar: MatSnackBar,
    private store: Store<State>
  ) { }

  ngOnInit() {
    this.release = this.route.snapshot.data['release'];
    this.displayedColumns = [ 'position', 'name', 'genre', 'length'];
    this.dataSource = new MatTableDataSource(this.route.snapshot.data['release'].tracks);
    this.dataSource.sort = this.sort;
  }

  onRowClicked(row) {
    console.log(row);
  }

  openSnackBar(message: string, action: string) {
    //this.snackBar.openFromComponent(FooterComponent);
    // this.snackBar.open(message, action, {
    //   //duration: 20000,
    //   panelClass: 'albumart-snackbar-container'
    // });
  }

  addAndPlayTrack(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueueAndStartPlaying([track]));
  }

  addTrackToQueue(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueue([track]));
  }

  playRelease(release: Release) {
    this.store.dispatch(new playerActions.AddTracksToQueueAndStartPlaying(release.tracks));
  }
}
