import { NgModule } from "@angular/core";
import { ReleasesComponent } from "./release-list/releases.component";
import { ReleaseService } from "./release.service";
import { SharedModule } from "../shared/shared.module";
import { RouterModule } from "@angular/router";
import { ReleaseDetailsComponent } from "./release-details/release-details.component";
import { MaterialModule } from "../material.module";
import { ReleaseResolver } from "./release-resolver.service";

@NgModule({
    imports: [ 
        SharedModule,
        MaterialModule,
        RouterModule.forChild([
            { path: "releases", component: ReleasesComponent, data: { animation: 'releases' } }, 
            {
                path: "releases/:id",
                component: ReleaseDetailsComponent,
                resolve: { release: ReleaseResolver }, 
                data: { animation: 'inf' }
            }            
        ])    
    ],
    declarations: [
        ReleasesComponent,
        ReleaseDetailsComponent,
    ],
    providers: [
        ReleaseService,
        ReleaseResolver
    ]
})
export class ReleaseModule { }
