import { Component, OnInit } from '@angular/core';
import { ReleaseService } from '../release.service';
import { Release } from '../release';
import { State } from '../../state/app.state';
import { Store } from '@ngrx/store';
//import { PlayerActionTypes } from '../music-library/state/music.library.actions';
import * as playerActions from '../../music-library/state/music.library.actions';
import { ITrack } from '../../music-library/shared/track.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-releases',
  templateUrl: './releases.component.html',
  styleUrls: ['./releases.component.css']
})
export class ReleasesComponent implements OnInit {
  
  releases$: Observable<Release[]>;

  constructor(private releaseService: ReleaseService,
    private store: Store<State>) { }

  ngOnInit() {
    this.releases$ = this.releaseService.getReleases();
  }

  addTrackToQueue(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueue([track]));
  }
  
  addAndPlayTrack(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueueAndStartPlaying([track]));
  }

  addReleaseToQueue(release: Release) {
    this.store.dispatch(new playerActions.AddTracksToQueue(release.tracks));
  }

  playRelease(release: Release) {
    this.store.dispatch(new playerActions.AddTracksToQueueAndStartPlaying(release.tracks));
  }
}
