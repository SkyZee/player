import { ITrack } from "../music-library/shared/track.model";

export interface Release {
    id: number;
    name: string;
    releaseDate: Date; 
    tracks: ITrack[];
    coverImgSrc: string;
    labelId: number;
    labelName: string;
}