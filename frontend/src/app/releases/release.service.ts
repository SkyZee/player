import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of, pipe } from "rxjs";
import { Release } from "./release";
import { tap } from "rxjs/operators";
import { environment } from "src/environments/environment";

@Injectable()
export class ReleaseService {
    private baseUrl = environment.endpointBase + 'releases';
    releases: Release[];

    constructor(private http: HttpClient) {}

    getReleases(): Observable<Release[]> {
        if (this.releases) {
            return of(this.releases);
        }
        return this.http.get<Release[]>(this.baseUrl)
            .pipe(
                tap(data => this.releases = data),
            );
    }

    getRelease(id: number): Observable<Release> {
        const url = `${this.baseUrl}/${id}`;
        return this.http.get<Release>(url)
            .pipe(
                tap(data => data)
                ,
                tap(data => console.log(data))
            );

    }
}