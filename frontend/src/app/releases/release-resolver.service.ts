import { Injectable } from "@angular/core";
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from "@angular/router";
import { Release } from "./Release";
import { Observable, of } from "rxjs";
import { ReleaseService } from "./release.service";
import { tap } from "rxjs/operators";

@Injectable()
export class ReleaseResolver implements Resolve<Release> {

    constructor(private router: Router, private releaseService: ReleaseService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Release> {
        let id = route.params['id'];
        if(isNaN(id)) {
            console.log(`Release id was not a number: ${id}`);
            this.router.navigate(['/releases']);
            return of(null);
        }
        return this.releaseService.getRelease(+id)
            .pipe(tap(release => {
                if (release) {
                    console.log(id);
                    return release;
                }
                this.router.navigate(['/releases']);
                return null;
            }));
            
    }
}