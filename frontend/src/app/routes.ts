import { Routes } from "@angular/router";
import { ArtistsComponent } from "./artists/artist-list/artists.component";
import { MusicLibComponent } from "./music-library/music-lib/music-lib.component";
import { GenreListComponent } from "./genres/genre-list/genre-list.component";
import { GenreDetailsComponent } from "./genres/genre-details/genre-details.component";
import { ArtistProfileComponent } from "./artists/artist-profile/artist-profile.component";

export const appRoutes: Routes = [
    { path: 'home', component: MusicLibComponent, data: { animation: '' } },
    { path: "artists",  component: ArtistsComponent, data: { animation: '' } },
    { path: "artists/:id", component: ArtistProfileComponent, data: { animation: 'art' } },
    { path: "genres", component: GenreListComponent, data: { animation: '' } },
    { path: "genres/:id", component: GenreDetailsComponent, data: { animation: 'deta' }},
]