import { Artist } from "../../artists/artist";

export interface ITrack {
    id: number,
    name: string,
    artistId: number;
    artist: string,
    label: string,
    genre: string,
    releasedDate: Date,
    src?: string,
    length?: string,
    albumArtSrc?: string
}