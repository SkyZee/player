import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { Observable, of } from "rxjs";
import { Action } from "@ngrx/store";
import * as libraryActions from './music.library.actions';
import { mergeMap, map, catchError, tap } from "rxjs/operators";
import { Playlist } from "src/app/playlists/shared/playlist";
import { PlaylistService } from "src/app/playlists/shared/playlist.service";

@Injectable()
export class MusicLibraryEffects {

    constructor(private actions$: Actions, private playlistService: PlaylistService) { }


    @Effect()    
    loadPlaylist$: Observable<Action> = this.actions$.pipe(
      
      ofType(libraryActions.PlayerActionTypes.LoadPlaylists),
         mergeMap(action =>
           this.playlistService.getPlaylists().pipe(
            tap(() => console.log('loadPlaylist')),
             map(playlists => (new libraryActions.LoadPlaylistsSuccess(playlists))),
             catchError(err => of(new libraryActions.LoadPlaylistsFail(err)))
           )
         )
    );

    @Effect()    
    updatePlaylist$: Observable<Action> = this.actions$.pipe(
        ofType(libraryActions.PlayerActionTypes.UpdatePlaylist),
        map((action: libraryActions.UpdatePlaylist) => action.payload),
        mergeMap((playlist: Playlist) =>
          this.playlistService.createPlaylist(playlist).pipe(
            map(products => (new libraryActions.UpdatePlaylistSuccess(products[0]))),
            catchError(err => of(new libraryActions.UpdatePlaylistFail(err)))
          )
        )
      );
    // ofType(libraryActions.PlayerActionTypes.UpdatePlaylist).
    //     pipe(tap((action) => console.log(`Received ${action.type}`)));    
    // tap(() => { debugger }),
        //ofType(libraryActions.PlayerActionTypes.UpdatePlaylist)
        //.
        //  pipe(
        //      tap(() => { console.log(`@Effect() updatePlaylist$`)})
        //     map((action: libraryActions.UpdatePlaylist) => action.payload),
        //     mergeMap((playlist: Playlist) =>
        //         this.playlistService.updatePlaylist(playlist).pipe(
        //             map(updatedPlaylist => (new libraryActions.UpdatePlaylistSuccess(updatedPlaylist))),
        //             catchError(err => of(new libraryActions.UpdatePlaylistFail(err)))
        //         )
        //     )
        //  );

    // @Effect()
    // createPlaylist$: Observable<Action> = this.actions$.pipe(
    //     // ofType(libraryActions)
    // )
}