import { ITrack } from "../shared/track.model";
import * as fromRoot from '../../state/app.state';
import { PlayerActionTypes, PlayerActions, QueueActionTypes, QueueActions, RemoveTrackFromQueue } from "./music.library.actions";
import { createSelector, createFeatureSelector } from "@ngrx/store";
import { Playlist } from "src/app/playlists/shared/playlist";

export interface State extends fromRoot.State {
    player: PlayerState
}

export interface PlayerState {
    queue: ITrack[];
    currentTrack: ITrack;
    currentTrackIdx: number,
    isPlaying: boolean;
    isTrackUploaded: boolean;
    playlists: Playlist[];
}

const initialState: PlayerState = {
    queue: [],
    currentTrack: null,
    currentTrackIdx: 0,
    isPlaying: false,
    isTrackUploaded: false,
    playlists: []
};

const getPlayerFeatureState = createFeatureSelector<PlayerState>('player');

export const getCurrentTrack = createSelector(
    getPlayerFeatureState, 
    state => state.currentTrack
);

export const getPlaylists = createSelector(
    getPlayerFeatureState,
    state => state.playlists
);

export function reducer(state = initialState, action: PlayerActions | QueueActions): PlayerState {
    switch (action.type) {
        case PlayerActionTypes.LoadPlaylistsSuccess:
         console.log(action.payload);
            return {
                ...state,
                playlists: action.payload
            }
        // remove all tracks from queue and unset currentTrack
        case QueueActionTypes.ClearQueue:
            return {
                ...state,
                queue: []
            }

        // remove a track from the queue
        case PlayerActionTypes.RemoveTrackFromQueue:
            return {
                ...state,
                queue: immutableDelete(state.queue, action.payload)
            }

        // set current track means start playing it
        case PlayerActionTypes.SetCurrentTrack:

            return {
                ...state,
                currentTrack: action.payload// Object.assign({}, action.payload.track),
                // currentTrackIdx: action.payload.idx // TODO: replace to actual idx
            }

        case PlayerActionTypes.PlayNextTrack:
            let nextIdx = (state.currentTrackIdx == state.queue.length - 1) ? 0 : state.currentTrackIdx + 1;
            return {
                ...state,
                currentTrackIdx:  nextIdx,
                currentTrack: state.queue[nextIdx]
            }

        case PlayerActionTypes.PlayPrevTrack:
            let prevIdx = state.currentTrackIdx == 0 ? state.queue.length - 1 : state.currentTrackIdx - 1;
            return {
                ...state,
                currentTrackIdx: prevIdx,
                currentTrack: state.queue[prevIdx]
            }

        // add tracks to the end of queue
        case PlayerActionTypes.AddTracksToQueue:
            return {
                ...state,
                queue: [
                    ...state.queue,
                    ...action.payload
                ]
            }

        // add tracks to the end of queue and play the first added track    
        case PlayerActionTypes.AddTracksToQueueAndStartPlaying:
            return {
                ...state,
                queue: [
                    ...state.queue,
                    ...action.payload
                ],
                currentTrack: action.payload[0]
            }
        case PlayerActionTypes.StartPlaying:
            var upd = Object.assign({}, state, { isPlaying: true });
            return upd;
        case PlayerActionTypes.Pause:
            return Object.assign({}, state, { isPlaying: false });
        default:
            return state;
    }
}

// Helpers for immutable arrays
function immutableDelete(arr, index) {
    return arr.slice(0, index).concat(arr.slice(index + 1))
}