import { Action } from "@ngrx/store";
import { ITrack } from "../shared/track.model";
import { Genre } from "../../genres/genre";
import { Playlist } from "src/app/playlists/shared/playlist";

export enum PlayerActionTypes {
    SetCurrentTrack = '[Track] Set Current Track',
    PlayNextTrack = '[Track] Play next track',
    PlayPrevTrack = '[Track] Play prev track',
    LoadPlaylists = '[Playlist] Load',
    LoadPlaylistsSuccess = '[Playlist] Load Success',
    LoadPlaylistsFail = '[Playlist] Load fail',
    AddTracksToQueue = '[Track] Add a list of tracks To Queue',
    AddTracksToQueueAndStartPlaying = '[Track] Add release to queue and start playing',
    // player
    MutePlayer = '[Player] Mute Player',
    StartPlaying = '[Player] Start Playing',
    Pause = '[Player] Pause Player',
    RemoveTrackFromQueue = '[Queue] Remove Track From Queue',
    LoadGenres = '[Genres] Load',
    LoadGenresSuccess = '[Genres] Load Success',
    UpdatePlaylist = '[Playlist] Update Playlist',
    UpdatePlaylistSuccess = '[Playlist] Update Playlist Success',
    UpdatePlaylistFail = '[Playlist] Update Playlist Fail'
}


export enum QueueActionTypes {
    ClearQueue = '[Queue] Clear Queue'
}

export class LoadPlaylists implements Action {
    readonly type = PlayerActionTypes.LoadPlaylists;


}

export class LoadPlaylistsSuccess implements Action {
    readonly type = PlayerActionTypes.LoadPlaylistsSuccess;

    constructor(public payload: Playlist[]) { }
}

export class LoadPlaylistsFail implements Action {
    readonly type = PlayerActionTypes.LoadPlaylistsFail;

    constructor(public payload: string) { }
}

export class ClearQueue implements Action {
    readonly type = QueueActionTypes.ClearQueue;
}

export class SetCurrentTrack implements Action {
    readonly type = PlayerActionTypes.SetCurrentTrack;

    constructor(public payload: any) {};
}

export class LoadTracks implements Action {
    readonly type = PlayerActionTypes.LoadPlaylists;
}

export class LoadSuccess implements Action {
    readonly type = PlayerActionTypes.LoadPlaylistsSuccess;

    constructor(public payload: Playlist[]) {};
}

export class LoadGenres implements Action {
    readonly type = PlayerActionTypes.LoadGenres;

    // constructor(public payload: Genre[]) {};
}

export class LoadGenresSuccess implements Action {
    readonly type = PlayerActionTypes.LoadGenresSuccess;

    constructor(public payload: Genre[]) { }
}

export class AddTracksToQueue implements Action {
    readonly type = PlayerActionTypes.AddTracksToQueue;

    constructor(public payload: ITrack[]) {};
}

export class AddTracksToQueueAndStartPlaying implements Action {
    readonly type = PlayerActionTypes.AddTracksToQueueAndStartPlaying;

    constructor(public payload: ITrack[]) {};
}

export class MutePlayer implements Action {
    readonly type = PlayerActionTypes.MutePlayer;

    constructor(public payload: boolean) {};
}

export class StartPlaying implements Action {
    readonly type = PlayerActionTypes.StartPlaying;

    constructor(public payload: boolean) {};
}

export class PlayNextTrack implements Action {
    readonly type = PlayerActionTypes.PlayNextTrack
}

export class PlayPrevTrack implements Action {
    readonly type = PlayerActionTypes.PlayPrevTrack
}

export class PausePlayer implements Action {
    readonly type = PlayerActionTypes.Pause;

    constructor(public payload: boolean) {}
}

export class RemoveTrackFromQueue implements Action {
    readonly type = PlayerActionTypes.RemoveTrackFromQueue;
    
    constructor(public payload: number) { };
}

export class UpdatePlaylist implements Action {
    readonly type = PlayerActionTypes.UpdatePlaylist;
    constructor(public payload: any) { }
}

export class UpdatePlaylistSuccess implements Action {
    readonly type = PlayerActionTypes.UpdatePlaylistSuccess;
    constructor(public payload: Playlist) { }
}

export class UpdatePlaylistFail implements Action {
    readonly type = PlayerActionTypes.UpdatePlaylistFail;
    constructor(public payload: string) { }
}

export type PlayerActions = SetCurrentTrack 
    | PlayNextTrack
    | PlayPrevTrack
    | LoadTracks
    | LoadSuccess
    | LoadGenres
    | LoadGenresSuccess
    | AddTracksToQueue
    | AddTracksToQueueAndStartPlaying
    | StartPlaying
    | PausePlayer;

export type QueueActions = ClearQueue | RemoveTrackFromQueue;