import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { reducer } from './state/music-library.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('library', reducer)
  ],
  declarations: []
})
export class MusicLibraryModule { }
