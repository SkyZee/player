import { ITrack } from "./shared/track.model";
import { Injectable } from "@angular/core";

@Injectable()
export class CurrentPlaylist {
    
    public tracks: ITrack[] = [];

    addTrack(track: ITrack) {
        this.tracks.push(track);
    }

    removeTrack(idx: number) {
        this.tracks.splice(idx, 1);
    }

    clear() {
        this.tracks = [];
    }

    trackCount(): number {
        return this.tracks.length;
    }
}