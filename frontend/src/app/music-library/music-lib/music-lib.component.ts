import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ITrack } from '../shared/track.model';

import * as fromLibrary from '../state/music-library.reducer';
import * as playerActions from '../state/music.library.actions';
import { State } from '../../state/app.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'music-lib',
  templateUrl: './music-lib.component.html',
  styleUrls: ['./music-lib.component.css']
})
export class MusicLibComponent implements OnInit {
  playlists: any;
  tracks$: Observable<ITrack[]>;

  constructor(private store: Store<State>
  ) { }

  ngOnInit() {
  }

  trackSelected(track: ITrack): void {
    console.log("select track: " + track.name);
    this.store.dispatch(new playerActions.SetCurrentTrack(track));
    this.store.dispatch(new playerActions.StartPlaying(true));
  }

  trackAddedToQueue(track: ITrack): void {
    this.store.dispatch(new playerActions.AddTracksToQueue([track]));
  }
}
