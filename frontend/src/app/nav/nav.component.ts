import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  @Output()
  toggled = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  onTurntable() {
    this.toggled.emit(true);
  }
}
