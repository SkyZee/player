import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { StoreModule, Store } from "@ngrx/store";

import { AppComponent } from './app.component';
import { PlayerComponent } from './player/player.component';
import { QueueComponent } from './queue/queue.component';
import { MusicLibComponent } from './music-library/music-lib/music-lib.component';
import { reducer } from './music-library/state/music-library.reducer';
import { HeaderComponent } from './header/header.component';

import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { NavComponent } from './nav/nav.component';

import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { CurrentPlaylist } from './music-library/current-playlist';
import { ReleaseModule } from './releases/release.module';
import { GenreModule } from './genres/genre.module';
import { LabelModule } from './labels/label.module';
import { ArtistModule } from './artists/artist.module';
import { PlaylistModule } from './playlists/playlist.module';
import { MaterialModule } from './material.module';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PlayerV2Component } from './player-v2/player-v2.component';
import { LibrarySidenavComponent } from './library-sidenav/library-sidenav.component';
import { MusicLibraryEffects } from './music-library/state/music-library.effects';
import { NotificationService } from './shared/notification.service';
// import { MusicLibraryEffects } from './music-library/state/music-library.effects';

@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    QueueComponent,
    MusicLibComponent,
    HeaderComponent,
    NavComponent,
    FooterComponent, 
    PlayerV2Component,
    LibrarySidenavComponent
    
  ],
  imports: [
    DragDropModule,
    MaterialModule,
    ReleaseModule,  
    ArtistModule,    
    GenreModule,    
    LabelModule,  
    BrowserModule,
    PlaylistModule,
    BrowserAnimationsModule,
    FormsModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature('library', reducer),
    EffectsModule.forRoot([
       MusicLibraryEffects
    ]),
    StoreDevtoolsModule.instrument({
      name: 'SkyZee Player App DevTools',
      maxAge: 25
    }),
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
    
  ],
  providers: [ CurrentPlaylist, NotificationService ],
  bootstrap: [ AppComponent ],
  
})
export class AppModule { }
