import { NgModule } from "@angular/core";
import { MatExpansionModule, MatButtonModule, MatIconModule, MatInputModule, MatListModule, MatCardModule, MatTabsModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatToolbarModule, MatSidenavModule, MatGridTile, MatGridListModule, MatBottomSheet, MatTableModule, MatPaginatorModule, MatMenuModule, MatSortModule, MatSnackBar, MatSnackBarContainer, MatSnackBarModule, MatProgressBarModule, MatSliderModule, MatTooltipModule } from "@angular/material";

@NgModule({
    imports: [
        MatButtonModule, 
        MatCardModule,
        MatExpansionModule, 
        MatIconModule, 
        MatInputModule,
        MatListModule,
        MatTabsModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatToolbarModule,
        MatSidenavModule,
        MatGridListModule,
        MatTableModule,
        MatPaginatorModule,
        MatMenuModule,
        MatSortModule,
        MatSnackBarModule,
        MatProgressBarModule,
        MatSliderModule,
        MatSnackBarModule,
        MatTooltipModule
    ],
    exports: [
        MatButtonModule, 
        MatCardModule,
        MatExpansionModule, 
        MatIconModule, 
        MatInputModule,
        MatListModule,
        MatTabsModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatToolbarModule,
        MatSidenavModule,
        MatGridListModule,
        MatTableModule,
        MatPaginatorModule,
        MatMenuModule,
        MatSortModule,
        MatSnackBarModule,
        MatProgressBarModule,
        MatSliderModule,
        MatSnackBarModule,
        MatTooltipModule
    ],
    providers: [MatBottomSheet, MatSnackBar]
})
export class MaterialModule { }
