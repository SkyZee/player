import { Component, OnInit } from '@angular/core';
import { ITrack } from '../music-library/shared/track.model';
import * as fromPlayer from '../music-library/state/music-library.reducer';
import { Store, select } from '@ngrx/store';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import * as playerActions from '../music-library/state/music.library.actions';
import { Playlist } from '../playlists/shared/playlist';

@Component({
  selector: 'queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.css']
})
export class QueueComponent implements OnInit {
  queue: ITrack[];
  queueTitle: string = 'All tracks';
  playlistName: string;

  // drop(event: CdkDragDrop<string[]>) {
  //   moveItemInArray(this.queue, event.previousIndex, event.currentIndex);
  // }
  constructor(private store: Store<fromPlayer.PlayerState>) { }

  ngOnInit() {
    // this.store.pipe(select('library')).subscribe(
    //   lib => {
    //       this.queue = lib.queue;
    //   }
    // );
  }

  clearQueue() {
    this.store.dispatch(new playerActions.ClearQueue());
  }

  updatePlaylist() {
    //var tr = this.queue.map(t => t.id);
    //console.log(tr);
    let pl = {
        id: 0,
        name: this.playlistName,
        tracks: this.queue.map(t => t.id)
      };
      console.log(pl);
    // let pl: Playlist = {
    //   id: 0,
    //   name: 'stupid playlist',
    //   description: 'and some description here',
    //   tracks: []
    // };
    this.store.dispatch(new playerActions.UpdatePlaylist(pl))
  }

  playTrack(track: ITrack, idx: number) {
    console.log(`play track: ${track} ${idx}`)
    // this.store.dispatch(new playerActions.SetCurrentTrack({track: track, idx: idx}));
    this.store.dispatch(new playerActions.SetCurrentTrack(track));
  }

  removeTrack(idx: number) {
    this.store.dispatch(new playerActions.RemoveTrackFromQueue(idx));
  }

}
