import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class NotificationService {
    public notification: Subject<string> = new Subject();
}