import { PipeTransform, Pipe } from "@angular/core";
import { isUndefined } from "util";

@Pipe({name: 'groupAlphabetically'})
export class GroupAlphabeticallyPipe implements PipeTransform {
    transform(collection: Array<any>, prop: string = 'name'): Array<any> {

        if (!collection) {
            return null;
        }
        // collection.sort();
        const arr: { [key: string]: Array<any>} = {};

        for (const value of collection) {

            const field: any = value[prop];
            const firstLetter = field.charAt(0).toUpperCase();

            if (isUndefined(arr[firstLetter])) {
                arr[firstLetter] = [];
            
            };

            arr[firstLetter].push(value);
        }
        
        return Object.keys(arr).sort().map(key => ({ key, 'value' : arr[key]}));
    }
}