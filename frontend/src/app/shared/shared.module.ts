import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GroupAlphabeticallyPipe } from "./group-alphabetically.pipe";

@NgModule({
    declarations: [ GroupAlphabeticallyPipe ],
    imports: [ CommonModule, FormsModule, ReactiveFormsModule ],
    exports: [ CommonModule, FormsModule, ReactiveFormsModule, GroupAlphabeticallyPipe ]
})
export class SharedModule { }

