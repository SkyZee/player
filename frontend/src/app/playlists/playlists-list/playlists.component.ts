import { Component, OnInit } from '@angular/core';
import { Playlist } from '../shared/playlist';
import { PlaylistService } from '../shared/playlist.service';
import { Observable } from 'rxjs';
import { ITrack } from 'src/app/music-library/shared/track.model';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/state/app.state';
import * as playerActions from '../../music-library/state/music.library.actions';
import * as fromMusicLibrary from '../../music-library/state/music-library.reducer';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.css']
})
export class PlaylistsComponent implements OnInit {
  playlists$: Observable<Playlist[]>;

  constructor(
    private playlistService: PlaylistService, 
    private store: Store<fromMusicLibrary.State>
    ) { }

  ngOnInit() {
    this.store.dispatch(new playerActions.LoadPlaylists());
    this.playlists$ = this.playlistService.getPlaylists();
  }

  addTrackToQueue(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueue([track]));
  }
  
  addAndPlayTrack(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueueAndStartPlaying([track]));
  }

  playPlaylist(id: number) {
  }

  addPlaylistToQueue(id: number) {
    console.log(`addPlaylistToQueue ${id}`);
  }

  goToRelease(id: number) {
    console.log(`navigate to release ${id}`);
  }
}
