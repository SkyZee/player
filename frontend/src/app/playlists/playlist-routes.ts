import { Routes } from "@angular/router";
import { PlaylistsComponent } from "./playlists-list/playlists.component";
import { PlaylistNewComponent } from "./playlist-new/playlist-new.component";

export const playlistRoutes: Routes = [
    { path: "playlists", component: PlaylistsComponent, data: { animation: '' } },
    { path: "playlists/new", component: PlaylistNewComponent, data: { animation: '' } },
]