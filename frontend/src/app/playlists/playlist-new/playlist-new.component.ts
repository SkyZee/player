import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Playlist } from '../shared/playlist';
import { PlaylistService } from '../shared/playlist.service';


@Component({
  selector: 'playlist-new',
  templateUrl: './playlist-new.component.html',
  styleUrls: ['./playlist-new.component.css']
})
export class PlaylistNewComponent implements OnInit {
  playlist: Playlist;
  playlistNewForm: FormGroup;
  errorMessage: any;

  constructor(private fb: FormBuilder, 
    private playlistService: PlaylistService, 
    private router: Router) { }

  ngOnInit() {
    
    this.playlistNewForm = new FormGroup({});
    //  this.fb.group({
    //   name: ['', Validators.required],
    //   description: ['', Validators.required]
    // });
  }

  get f() { return this.playlistNewForm.controls; }

  revert() {
    console.log('revert');
  }

  onSubmit() {
    if (this.playlistNewForm.invalid) {
      return;
    }
    console.log(this.f.title.errors);
  }

  savePlaylist() {
    debugger;
    if (this.playlistNewForm.dirty && this.playlistNewForm.valid) {
      let p = Object.assign({}, this.playlist, this.playlistNewForm.value);
      this.playlistService.savePlaylist(p).subscribe(
        () => this.onSaveComplete(),
        (error: any) => this.errorMessage = <any>error
      );
    } else if (!this.playlistNewForm.dirty) {
      this.onSaveComplete();
    }    
  }

  onSaveComplete() {
    this.playlistNewForm.reset();
    this.router.navigate(['/playlists']);
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json() || 'Server error')
  }
}
