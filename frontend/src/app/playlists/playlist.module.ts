import { NgModule } from "@angular/core";
import { PlaylistsComponent } from "./playlists-list/playlists.component";
import { PlaylistNewComponent } from "./playlist-new/playlist-new.component";
import { MaterialModule } from "../material.module";
import { SharedModule } from "../shared/shared.module";
import { RouterModule } from "@angular/router";
import { playlistRoutes } from "./playlist-routes";
import { PlaylistService } from "./shared/playlist.service";
// import { EffectsModule } from "@ngrx/effects";
// import { MusicLibraryEffects } from '../music-library/state/music-library.effects';

@NgModule({
    imports: [
        MaterialModule,
        SharedModule,
        RouterModule.forChild(playlistRoutes),
        // EffectsModule.forFeature(
        //     [MusicLibraryEffects]
        // )
    ],
    providers: [PlaylistService],
    declarations: [
        PlaylistsComponent,
        PlaylistNewComponent,
    ]
})
export class PlaylistModule { }
