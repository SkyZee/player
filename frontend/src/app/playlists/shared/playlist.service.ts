import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Playlist } from "./playlist";
import { Observable, throwError } from "rxjs";
import { map, tap, catchError } from 'rxjs/operators';
import { environment } from "src/environments/environment";
import { NotificationService } from "src/app/shared/notification.service";

@Injectable()
export class PlaylistService {
    private baseUrl = environment.endpointBase + 'playlists';
    playlists: Playlist[];

    constructor(
        private http: HttpClient,
        private notificationService: NotificationService
        ) { }

    getPlaylists() : Observable<Playlist[]> {
        // this.notificationService.notification.next('Hey');
        return this.http.get<Playlist[]>(this.baseUrl)
        .pipe(
            tap((playlists: Playlist[]) => this.playlists = playlists),// tap(data => data),
            // catchError(this.handleError)
        );
    }

    deletePlaylist(id: number): any {
        const url = `${this.baseUrl}/${id}`;
        return this.http.delete(url)
         .pipe(
             tap(data => console.log(`deletePlaylist: ${id}`)),
            //  catchError(this.handleError)
        );
    }

    savePlaylist(playlist: Playlist): Observable<Playlist> {
        if (playlist.id === 0) {
            return this.createPlaylist(playlist);
        }
        return this.updatePlaylist(playlist);
        return this.http.post<Playlist>(this.baseUrl, playlist);
    }

    createPlaylist(playlist: Playlist): Observable<Playlist> {
        return this.http.post<Playlist>(this.baseUrl, playlist)
            .pipe(
                tap(data => console.log('createPlaylist: '+ JSON.stringify(data))),
                // catchError(this.handleError)
            );
    }

    updatePlaylist(playlist: Playlist): Observable<Playlist> {
        const url = `${this.baseUrl}/${playlist.id}`;
        const httpOptions = {
            headers: new HttpHeaders({
              'Access-Control-Allow-Headers':  '*'
            })
          };
        return this.http.put(url, playlist, httpOptions)
            .pipe(
                tap(() => console.log(`updatePlaylist: ${playlist.id}`)),
                map(() => playlist),
                // catchError(this.handleError)
                );
    }

    private handleError(err: HttpErrorResponse) {
        // debugger;
        // TODO send to backend
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${err.message}`;
        } else {
            errorMessage = `Backend returned code ${err.status}: ${err.message}`;                
        }
        this.notificationService.notification.next('Something is wrong with playlists. Please retry later');
        // return throwError(errorMessage);
    }    
}