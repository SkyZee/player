export interface Playlist {
    id: number,
    name: string,
    description: string,
    tracks: TrackForPlaylist[]
}

export interface TrackForPlaylist {
    id: number,
    name: string,
    artist: string,
    length: number,
    releaseId: number
}