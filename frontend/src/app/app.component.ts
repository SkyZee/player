import { Component, ViewChild } from '@angular/core';
import { routerTransition } from './routerTransition';
import { MatSidenav } from '@angular/material/sidenav';
import { NotificationService } from './shared/notification.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  animations: [ routerTransition ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  @ViewChild('sidenav', { static: false})
  private sidenav: MatSidenav;

  constructor(public notificationService: NotificationService, public snackBar: MatSnackBar) {
    this.notificationService.notification.subscribe(message => {
      snackBar.open(message, null, {
        duration: 1500,

      });
    });
   }

  // change the animation state
  getRouteAnimation(outlet) {
    return outlet.activatedRouteData.animation
  }

  onToggled(open: boolean) {
    this.sidenav.toggle();
  }
}

