import { ITrack } from "../music-library/shared/track.model";

export interface Artist {
    id: number;
    name: string;
    tracks: ITrack[];
}