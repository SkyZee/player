import { Component, OnInit } from '@angular/core';
import { ArtistService } from '../artist.service';
import { Artist } from '../artist';
import { Observable } from 'rxjs';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.css']
})
export class ArtistsComponent implements OnInit {
  artists: Artist[];
  errorMessage: string;

  constructor(
    private artistService: ArtistService, 
    private notificationService: NotificationService
    ) { }

  ngOnInit() {
    this.artistService.getArtists().subscribe((data) => {
      this.artists = data;
    }, (err) => {
      this.notificationService.notification.next(err);
    });
  }

}
