import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Artist } from '../artist';
import { ArtistService } from '../artist.service';
import { ITrack } from 'src/app/music-library/shared/track.model';
import { Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import * as playerActions from '../../music-library/state/music.library.actions';


@Component({
  selector: 'app-artist-profile',
  templateUrl: './artist-profile.component.html',
  styleUrls: ['./artist-profile.component.css']
})
export class ArtistProfileComponent implements OnInit {
  
  artist: Artist | null;

  constructor(private artistService: ArtistService,
    private route: ActivatedRoute, private store: Store<State>) { }

  ngOnInit() {
    this.artistService.getArtist(+this.route.snapshot.paramMap.get('id'))
      .subscribe(
        (artist: Artist) => this.artist = artist
      );
  }

  addTrackToQueue(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueue([track]));
  }
  
  addAndPlayTrack(track: ITrack) {
    this.store.dispatch(new playerActions.AddTracksToQueueAndStartPlaying([track]));
  }

}
