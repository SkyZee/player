import { Injectable } from "@angular/core";
import { Artist } from "./artist";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, of, throwError } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { NotificationService } from "../shared/notification.service";

@Injectable()
export class ArtistService {
    private baseUrl = environment.endpointBase + 'artists';
    artists: Artist[];

    constructor(private http: HttpClient, private notificationService: NotificationService) {}

    getArtists(): Observable<Artist[]> {
        
        if (this.artists) {
            return of(this.artists);
        }
        return this.http.get<Artist[]>(this.baseUrl)
            .pipe(
                tap( data => this.artists = data),
                catchError(this.handleError)
            );
        
    }

    private handleError(err: HttpErrorResponse) {
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            errorMessage = `Backend returned code ${err.status}: ${err.message}`;                
        }
        return throwError('Something is wrong. Please retry later');
    }    

    getArtist(id: number): Observable<Artist> {
        if (this.artists) {
            if (this.artists.some(a => a.id == id)) {
                return of(this.artists.find(a => a.id == id))
            }
        }
        return this.http.get<Artist>(this.baseUrl + "/" + id)
            .pipe(
                tap(data => {return data})                
            );
    }
}