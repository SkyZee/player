import { NgModule } from "@angular/core";
import { ArtistService } from "./artist.service";
import { ArtistsComponent } from "./artist-list/artists.component";
import { ArtistProfileComponent } from "./artist-profile/artist-profile.component";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { MaterialModule } from "../material.module";

@NgModule({
    imports: [ SharedModule, RouterModule, MaterialModule ],
    providers: [ArtistService],
    declarations: [ArtistsComponent, ArtistProfileComponent]
})
export class ArtistModule { }
