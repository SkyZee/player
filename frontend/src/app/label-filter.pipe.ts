import { PipeTransform, Pipe } from '@angular/core';
import { Label } from './labels/label';

@Pipe({
    name: 'labelFilter'
})
export class LabelFilterPipe implements PipeTransform {
    transform(value: Label[], filterBy: string): Label[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? value.filter((label: Label) => 
            label.name.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}