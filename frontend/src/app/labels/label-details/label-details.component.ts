import { Component, OnInit } from '@angular/core';
import { Label } from '../label';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'label-details',
  templateUrl: './label-details.component.html',
  styleUrls: ['./label-details.component.css']
})
export class LabelDetailsComponent implements OnInit {
  label: Label;
  
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.label = this.route.snapshot.data['label'];
  }
}
