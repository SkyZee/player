import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Label } from "./label";
import { Observable, of, throwError } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { NotificationService } from "../shared/notification.service";

@Injectable()
export class LabelService {
    private baseUrl = environment.endpointBase + 'labels';
    private labels: Label[];

    constructor(
        private http: HttpClient, 
        private notificationService: NotificationService
        ) { }

    getLabels(): Observable<Label[]> {
        if (this.labels) {
            return of(this.labels);
        }
        return this.http.get<Label[]>(this.baseUrl)
            .pipe(
                tap(data => this.labels = data),
                catchError(this.handleError)
            );
    }

    getLabel(id: number): Observable<Label> {
        if (id === 0) {
            return of(this.initializeLabel())
        }

        const url = `${this.baseUrl}/${id}`;
        return this.http.get<Label>(url)
            .pipe(catchError(err => this.handleError(err)));
    }
    
    deleteLabel(): Observable<Response> {
        return of(null);
    }

    saveLabel(label: Label): Observable<Label> {
        console.log('label');
        console.log(label);
        return this.http.post<Label>(this.baseUrl, label)
            .pipe(catchError(err => this.handleError(err)));
    }

    initializeLabel(): Label {
        return {
            id: 0,
            name: null,
            releases: []
        }
    }

    private handleError(err: HttpErrorResponse) {
        // TODO send to backend
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            errorMessage = `Backend returned code ${err.status}: ${err.message}`;                
        }
        this.notificationService.notification.next('Something is wrong with labels. Please retry later');
        return throwError(errorMessage);
    }


}