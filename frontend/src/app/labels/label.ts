import { Release } from "../releases/release";

export interface Label {
    id: number;
    name: string;
    releases: Release[];
}