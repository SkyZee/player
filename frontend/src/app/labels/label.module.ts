import { NgModule } from "@angular/core";
import { LabelListComponent } from "./label-list/label-list.component";
import { LabelDetailsComponent } from "./label-details/label-details.component";
import { LabelService } from "./label.service";
import { RouterModule } from "@angular/router";
import { MaterialModule } from "../material.module";
import { SharedModule } from "../shared/shared.module";
import { LabelResolver } from "./label-resolver.service";
import { LabelFilterPipe } from "../label-filter.pipe";

@NgModule({
    imports: [ 
        RouterModule.forChild([
            { path: 'labels', component: LabelListComponent },
            { 
                path: 'labels/:id', 
                component: LabelDetailsComponent, 
                resolve: { label: LabelResolver },
                data: { animation: '' }
            }
        ]), 
        MaterialModule, 
        SharedModule        
    ],
    providers:    [ 
        LabelService, 
        LabelResolver 
    ],
    declarations: [ 
        LabelListComponent, 
        LabelDetailsComponent, 
        LabelFilterPipe
    ]
})
export class LabelModule { }
