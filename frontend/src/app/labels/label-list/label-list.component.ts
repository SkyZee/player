import { Component, OnInit } from '@angular/core';
import { Label } from '../label';
import { LabelService } from '../label.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-label-list',
  templateUrl: './label-list.component.html',
  styleUrls: ['./label-list.component.css']
})
export class LabelListComponent implements OnInit {
  labels$: Observable<Label[]>;
  listFilter: string;

  constructor(private labelService: LabelService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.listFilter = this.route.snapshot.queryParams['filterBy'] || '';
    this.labels$ = this.labelService.getLabels();
  }

}
