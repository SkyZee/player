import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Label } from "./label";
import { Observable, of } from "rxjs";
import { map } from 'rxjs/operators';
import { LabelService } from "./label.service";

@Injectable()
export class LabelResolver implements Resolve<Label> {

    constructor(private labelService: LabelService, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Label> {

        let id = +route.params['id'];
        if (isNaN(id)) {
            console.log(`Label id was not a number: ${id}`);
            this.router.navigate(['/labels']);
            return of(null)
        }

        return this.labelService.getLabel(+id)
            .pipe(
                map(label => {
                    return label;
            }));
    }
}