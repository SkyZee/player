import { Component, OnInit } from '@angular/core';
import { ITrack } from '../music-library/shared/track.model';
import { Store, select } from '@ngrx/store';
import * as fromPlayer from '../music-library/state/music-library.reducer';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  currentTrack: ITrack;
  
  constructor(private store: Store<fromPlayer.PlayerState>) { }

  ngOnInit() {
    this.store.pipe(select('currentTrack')).subscribe(ct => {
      if (ct) {
        this.currentTrack = ct;
      }
    });
  }
}
